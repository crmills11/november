Survey.Survey.cssType = "bootstrap";

var json = {
    title: "Knowledge Test",
    showProgressBar: "bottom",
    showTimerPanel: "top",
    maxTimeToFinishPage: 10,
    maxTimeToFinish: 100,
    firstPageIsStarted: true,
    startSurveyText: "Start Quiz",
    pages: [
        {
            questions: [
                {
                    type: "html",
                    html: "You are about to start a true/flase quiz. <br/>You have 10 seconds for every question and 120 seconds for the whole survey of 10 questions.<br/>Please click on <b>'Start Quiz'</b> button when you are ready."
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: less stress and better health",
                    title: "Benefits of positive attitude include: less stress and better health",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: improved focus",
                    title: "Benefits of positive attitude include: improved focus",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: improved innovation and creativity",
                    title: "Benefits of positive attitude include: improved innovation and creativity",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: increased self-esteem and self-confidence",
                    title: "Benefits of positive attitude include: increased self-esteem and self-confidence",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: improved relationships",
                    title: "Benefits of positive attitude include: improved relationships",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: more energy",
                    title: "Benefits of positive attitude include: more energy",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: decreases motivation",
                    title: "Benefits of positive attitude include: decreases motivation",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: doesn’t improve teamwork",
                    title: "Benefits of positive attitude include: doesn’t improve teamwork",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Benefits of positive attitude include: you’re a less likable person",
                    title: "Benefits of positive attitude include: you’re a less likable person",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, 
    ],
    completedHtml: "<h4>You have answered correctly <b>{correctedAnswers}</b> questions from <b>{questionCount}</b>.</h4>"
};

var survey = new Survey.Model(json);

survey
  .onComplete
  .add(function(result) {
    document
      .querySelector('#surveyResult')
      .innerHTML = "result: " + JSON.stringify(result.data);
  });

$("#surveyElement").Survey({
  model: survey
});
